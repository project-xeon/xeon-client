try {
    const {ipcRenderer, remote} = require('electron');
    const fs = remote.require('fs-extra');
    const EventEmitter = remote.require('events');
    const {BrowserWindow, dialog, app} = remote;
    const Path = remote.require('path');
    const osPath = Path.join(app.getAppPath(), `os`);
    const Random = remote.require('random-js');
    const random = new Random(Random.engines.browserCrypto);
    const restrictedPaths = [
        Path.join(osPath, 'index.html'),
        Path.join(osPath, 'editor.html'),
        Path.join(osPath, '.git'),
        Path.join(osPath, '.gitignore'),
        Path.join(osPath, 'LICENSE'),
        Path.join(osPath, 'assets'),
        Path.join(osPath, 'README.md')
    ];
    Math.random = function(){
        return random.real(0, 1, true);
    }
    if(window.localStorage.getItem('color_scheme') == undefined){
        window.localStorage.setItem('color_scheme', 'rgb');
    }
    function checkRestricted(path){
        return !(restrictedPaths.some(el=>{
            return el == path;
        }));
    }
    function lockPath(path){
        var x1 = Path.sep == '/' ? new RegExp(/\/{2,}/g) : new RegExp(/\\{2,}/g);
        var x2 = Path.sep == '/' ? new RegExp(/\\/g) : new RegExp(/\//g);
        path = path.replace(x2, Path.sep).replace(x1, Path.sep);
        var p = Path.isAbsolute(path) ? Path.normalize(path) : Path.join(Path.sep, path);
        p = Path.join(osPath, p);
        if(!checkRestricted(p)) {
            throw new Error(`Access denied to resource: ${p}`);
        } else {
            return p;
        }
    }
    let system = {};
    system.cwd = '/';
    system.uuid = function () {
        var S4 = function() {
        return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    }
    system.escapeCode = function(text){
        return text.replace(/(?<!\\)"/g, '\\"').replace(/(?<!\\)'/g, "\\'").replace(/(?<!\\)`/g, '\\`').replace(/\\n/g, '\\\\n').replace(/\\t/g, '\\\\t').replace(/\\r/g, '\\\\r').replace(/\n/g, '\\n').replace(/\t/g, '\\t').replace(/\r/g, '\\r');
    }
    system.eval_callback = null;
    system.run_callback = null;
    system.editor = null;
    system.createBuffer = function(data){
        return Buffer.from(data);
    }
    system.crypto = {};
    const crypto = remote.require('crypto');
    system.crypto.createHash = function(alg){
        return crypto.createHash(alg);
    }
    system.filesystem = {};
    system.filesystem.path = Path;
    system.filesystem.dialog = dialog;
    const targz = remote.require('targz');
    const semver = remote.require('semver');
    system.filesystem.decompress = function(name, version, body){
        return new Promise((resolve, reject)=>{
            var path = lockPath('packages/'+name);
            var tempdir = lockPath('temp');
            var tempfile = Path.join(tempdir, system.uuid()+'.tar.gz');
            if(!fs.existsSync(path)) fs.mkdirSync(path);
            if(!fs.existsSync(tempdir)) fs.mkdirSync(tempdir);
            var hasher = system.crypto.createHash('SHA256');
            hasher.update(Buffer.from(body.tar));
            var hash = hasher.digest().toString('hex');
            if(hash != body.hash){
                reject(`Downloaded file failed checksum verification.\nRemote: ${body.hash}\nLocal: ${hash}`);
            } else {
                fs.writeFileSync(tempfile, Buffer.from(body.tar));
                targz.decompress({
                    src: tempfile,
                    dest: path
                }, (err)=>{
                    if(err) {
                        fs.removeSync(tempdir);
                        reject(err);
                    } else {
                        fs.removeSync(tempdir);
                        var pk = JSON.parse(fs.readFileSync(lockPath(`packages/${name}/package.json`)).toString());
                        var hooks = pk.hooks;
                        if(!hooks){
                            resolve({success: true, hooks: []});
                        } else {
                            var h = [];
                            if(hooks.preInstall) h.push(`packages/${name}/${hooks.preInstall}`);
                            if(hooks.postInstall) h.push(`packages/${name}/${hooks.postInstall}`);
                            resolve({success: true, hooks: h, failed: ()=>{
                                fs.removeSync(path);
                            }});
                        }
                    }
                });
            }
        });
    }
    system.filesystem.compress = function(package){
        return new Promise((resolve, reject)=>{
            try {
                var path = lockPath(`packages/${package}`);
                var p = lockPath(`packages`);
                var info = Path.join(path, 'package.json');
                var tempdir = lockPath('temp');
                var tempfile = Path.join(tempdir, system.uuid()+'.tar.gz');
                if(!fs.existsSync(path) || !fs.existsSync(info)){
                    reject('Package directory or package.json is missing.');
                } else {
                    var pk = fs.readFileSync(info).toString();
                    try {
                        pk = JSON.parse(pk);
                        if(!pk.name || !pk.version){
                            reject('Invalid package.json');
                        } else {
                            if(semver.valid(pk.version) == null){
                                reject('Invalid version style.\nVersion must be parsable by node-semver.');
                            } else {
                                if(!fs.existsSync(tempdir)) fs.mkdirSync(tempdir);
                                targz.compress({
                                    src: path,
                                    dest: tempfile
                                }, (err)=>{
                                    if(err) {
                                        fs.removeSync(tempdir);
                                        reject(err);
                                    }
                                    else {
                                        var data = fs.readFileSync(tempfile);
                                        fs.removeSync(tempdir);
                                        var hasher = system.crypto.createHash('sha256');
                                        hasher.update(data);
                                        var hash = hasher.digest().toString('hex');
                                        resolve({data: data, hash: hash, name: pk.name, version: pk.version});
                                    }
                                });
                            }
                        }
                    } catch(e){ 
                        reject(e);
                    }
                }
            } catch(e) {
                reject(e);
            }
        });
    }
    system.filesystem.fs = {
        readFileSync: function(path, encoding){
            path = lockPath(path);
            var d = fs.readFileSync(path);
            if(encoding != undefined && encoding != null){
                d = d.toString(encoding);
            }
            return d;
        },
        readFile: function(path, encoding){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.readFile(path, (err, d)=>{
                    if(err) reject(err);
                    else {
                        if(encoding != undefined && encoding != null){
                            d = d.toString(encoding);
                        }
                        resolve(d);
                    }
                });
            });
        },
        existsSync: function(path){
            path = lockPath(path);
            return fs.existsSync(path);
        },
        exists: function(path){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.exists(path, (exists)=>{
                    resolve(exists);
                });
            });
        },
        writeFileSync: function(path, data){
            path = lockPath(path);
            fs.writeFileSync(path, data);
        },
        writeFile: function(path, data){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.writeFile(path, data, (err)=>{
                    if(err) reject(err);
                    else resolve();
                });
            });
        },
        appendFileSync: function(path, data){
            path = lockPath(path);
            fs.appendFileSync(path, data);
        },
        appendFile: function(path, data){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.appendFile(path, data, (err)=>{
                    if(err) reject(err);
                    else resolve();
                });
            });
        },
        readdirSync: function(path){
            path = lockPath(path);
            return fs.readdirSync(path);
        },
        readdir: function(path){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.readdir(path, (err, files)=>{
                    if(err) reject(err);
                    else resolve(files);
                });
            });
        },
        unlinkSync: function(path){
            path = lockPath(path);
            fs.unlinkSync(path);
        },
        unlink: function(path){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.unlink(path, (err)=>{
                    if(err) reject(err);
                    else resolve();
                });
            });
        },
        mkdirSync: function(path){
            path = lockPath(path);
            fs.mkdirSync(path);
        },
        mkdir: function(path){
            return new Promise((resolve, reject)=>{
                path = lockPath(path);
                fs.mkdir(path, (err)=>{
                    if(err) reject(err);
                    else resolve();
                });
            });
        }
    };
    Object.freeze(system.filesystem.fs);
    Object.freeze(system.filesystem.dialog);
    Object.freeze(system.filesystem);
    system.commands = new Map();
    system.events = new EventEmitter();
    system.events.network = new EventEmitter();
    system.events.commands = new EventEmitter();
    system.events.process = new EventEmitter();
    system.processes = new Map();
    system.systemd = new Map();
    system.network = new EventEmitter();
    system.network.sockets = new Map();
    system.network.servers = [];
    system.network.ssh = null;
    system.network.address = 'localhost';
    system.network.http = new Map();
    system.network.request = {};
    system.network.request.parseURI = function(uri){
        var reg = new RegExp(/(?:http:\/\/|https:\/\/|)([\w-_\.]*)\/?(.*)$/);
        var m = reg.exec(uri);
        var host = m[1];
        var url = encodeURIComponent(`/${m[2]}`);
        return {host: host, path: url};
    }
    system.network.request.post = function(opts = {uri: '', body: '', json: false, strictJSON: false}){
        return new Promise((resolve, reject)=>{
            opts = Object.assign({}, {uri: '', body: '', json: false}, opts);
            var uri = opts.uri;
            var method = 'POST';
            var body = opts.body;
            var json = opts.json;
            if(json) body = JSON.stringify(body);
            var parsed = system.network.request.parseURI(uri);
            var host = parsed.host;
            var path = parsed.path;
            var id = system.uuid();
            system.network.http.set(id, (res)=>{
                try {
                    res.body = Buffer.from(res.body)
                } catch(e){
    
                }
                if(json){
                    try {
                        res.body = JSON.parse(res.body.toString());
                    } catch(e) {
                        if(opts.strictJSON)
                            reject(new Error('Body was not valid JSON.'));
                        else
                            res.body = res.body.toString();
                    }
                    resolve([res, res.body]);
                } else {
                    resolve([res, res.body]);
                }
            });
            system.eval(`(async ()=>{
                net.send("${host}", 80, JSON.stringify({id: "${id}", request: {hostname: "${host}", method: "${method}", path: "${path}", body: JSON.stringify('${body}')}}));
            })()`);
        });
    }
    
    system.network.request.get = function(opts = {uri: '', body: '', json: false, strictJSON: false}){
        return new Promise((resolve, reject)=>{
            opts = Object.assign({}, {uri: '', body: '', json: false}, opts);
            var uri = opts.uri;
            var method = 'GET';
            var body = opts.body;
            var json = opts.json;
            if(json) body = JSON.stringify(body);
            var parsed = system.network.request.parseURI(uri);
            var host = parsed.host;
            var path = parsed.path;
            var id = system.uuid();
            system.network.http.set(id, (res)=>{
                try {
                    res.body = Buffer.from(res.body);
                } catch(e){
    
                }
                if(json){
                    try {
                        res.body = JSON.parse(res.body.toString());
                    } catch(e) {
                        if(opts.strictJSON)
                            reject(new Error('Body was not valid JSON.'));
                        else
                            res.body = res.body.toString();
                    }
                    resolve([res, res.body]);
                } else {
                    resolve([res, res.body]);
                }
            });
            system.eval(`(async ()=>{
                net.send("${host}", 80, JSON.stringify({id: "${id}", request: {hostname: "${host}", method: "${method}", path: "${path}", body: JSON.stringify('${body}')}}));
            })()`);
        });
    }
    
    system.parseArguments = function(args){
        var t = [];
        var buffer = [];
        args.forEach(arg=>{
            if((arg[0] == "'" || arg[0] == '"') && !(arg[arg.length-1] == '"' || arg[arg.length-1] == "'") && buffer.length == 0) {
                buffer.push(arg.substring(1));
            } else if(!(arg[0] == "'" || arg[0] == '"') && (arg[arg.length-1] == '"' || arg[arg.length-1] == "'") && buffer.length != 0){
                buffer.push(arg.substring(0, arg.length-1));
                t.push(buffer.join(' '));
                buffer = [];
            } else if((arg[0] == "'" || arg[0] == '"') && (arg[arg.length-1] == '"' || arg[arg.length-1] == "'")) {
                t.push(arg.substring(1, arg.length-1));
            } else if(buffer.length != 0){
                buffer.push(arg);
            } else {
                t.push(arg);
            }
        });
        args = null;
        return t;
    }
    system.resolve = function(path){
        if(Path.isAbsolute(path)){
            return path;
        } else {
            return Path.normalize(`${system.cwd}${system.cwd == '/' ? '' : '/'}${path}`).replace(/\\/g, '/');
        }
    }
    system.eval = function(code, callback){
        if(callback != undefined && typeof callback == 'function'){
            system.eval_callback = function(ret){
                system.eval_callback = null;
                callback(ret);
            }
        }
        system.send('eval', code);
    }
    system.run = function(path, callback){
        if(callback != undefined && typeof callback == 'function'){
            system.run_callback = function(ret){
                system.run_callback = null;
                callback(ret);
            }
        }
        system.send('run', path);
    }

    system.network.send = function(address, port, data){
        if(address != '127.0.0.1' && address != 'localhost' || 1 == 1){
            data = Buffer.from(data);
            system.eval(`(async ()=>{
                net.send("${address}", ${port}, ${JSON.stringify(data.toJSON().data)});
            })()`);
        } else {
            system.events.emit('inbound', {source: '127.0.0.1', port: port, buffer: data});
        }
    }

    class NetSocket extends EventEmitter{
        constructor(opts = {host: '', port: 0, timeout: 5000}, uid = system.uuid()){
            super();
            this.uid = uid;
            this.timeout = opts.timeout;
            this.internal = {
                to: null
            };
            this.host = opts.host == 'localhost' ? '127.0.0.1' : opts.host;
            this.port = opts.port;
            this.connected = false;
            this.once('connect', ()=>{
                this.connected = true;
                if(this.internal.to != null) {
                    clearTimeout(this.internal.to);
                    this.internal.to = null;
                    this.emit('connect');
                }
            });
        }
        Connect(){
            const self = this;
            var data = JSON.stringify({op: 'connect', uid: this.uid});
            this.internal.to = setTimeout(()=>{
                self.emit('error', 'Connection timed out.');
            }, this.timeout);
            system.network.send(this.host, this.port, data);
        }
        close(){
            if(this.connected == true){
                var data = JSON.stringify({op: 'close', uid: this.uid});
                system.network.send(this.host, this.port, data);
            } else {
                this.emit('close');
            }
        }
        write(data){
            if(this.connected == true){
                if(data instanceof Buffer){
                    data = Buffer.toJSON().data;
                }
                if(typeof data == 'object' && data != null) data = JSON.stringify(data);
                data = JSON.stringify({op: 'data', uid: this.uid, data: data});
                system.network.send(this.host, this.port, data);
            } else {
                
            }
        }
    }

    class SSHCon extends EventEmitter {
        constructor(socket){
            super();
            const self = this;
            this.socket = socket;
            this.resolves = new Map();
            this.socket.on('data', data=>{
                try {
                    var dobj = JSON.parse(data);
                    if(self.resolves.get(dobj.id)){
                        this.resolves.get(dobj.id)(dobj.data);
                    }
                } catch(e) {
                    this.socket.emit(error, e.message);
                }
            });
            this.socket.on('error', (e)=>{
                system.terminal.print(e.message || e);
            });
            this.socket.on('close', ()=>{
                this.emit('close');
            });
        }
        send(obj, resolve){
            if(obj.data == 'disconnect'){
                this.socket.close();
                resolve();
            }
            this.resolves.set(obj.id, (d)=>{
                this.resolves.delete(obj.id);
                resolve(d);
            });
            this.socket.write(JSON.stringify(obj));
        }
        close(){
            this.socket.close();
        }
    }

    class NetServer extends EventEmitter{
        constructor(opts = {port: 0, timeout: 30000}){
            super();
            this.timeout = opts.timeout;
            this.port = opts.port;
            this.sockets = new Map();
        }
        Listen(){
            system.network.servers[this.port] = this;
            this.emit('listen');
        }
        Close(){
            this.sockets.forEach(socket=>{
                socket.close();
            });
            this.emit('close');
        }
        Connect(source, uid){
            let socket = new NetSocket({host: source, port: this.port, timeout: 5000}, uid);
            socket.connected = true;
            this.sockets.set(uid, socket);
            socket.write = function(data){
                if(data instanceof Buffer){
                    data = Buffer.toJSON().data;
                }
                if(typeof data == "object" && data != null) data = JSON.stringify(data);
                data = JSON.stringify({return: 'data', uid: socket.uid, data: data});
                system.network.send(socket.host, socket.port, data);
            }
            socket.Connect = function(){
                var data = JSON.stringify({return: 'connect', uid: socket.uid});
                system.network.send(socket.host, socket.port, data);
            }
            socket.close = function(){
                var data = JSON.stringify({return: 'close', uid: socket.uid, data: data});
                system.network.send(socket.host, socket.port, data);
            }
            setTimeout(()=>{
                socket.Connect();
            }, 100);
            this.emit('connect', socket);
        }
        Inbound(source, data){
            try {
                let dobj = JSON.parse(data.toString());
                if(dobj.uid){
                    let socket = this.sockets.get(dobj.uid);
                    if(socket && socket.host == source){
                        if(dobj.op == 'close') {
                            socket.close();
                            socket.emit('close');
                            this.sockets.delete(socket.uid);
                        } else {
                            socket.emit('data', Buffer.from(dobj.data));
                        }
                    } else if(!socket){
                        this.Connect(source, dobj.uid);
                    } else {
                        
                    }
                }
            } catch(e) {
                console.log(e);
                this.emit('error', e);
            }
        }
    }

    system.network.createClient = function(opts = {host: '', port: 0, timeout: 5000}, cb){
        let socket = new NetSocket(opts);
        system.network.sockets.set(socket.uid, socket);
        socket.on('close', ()=>{
            system.network.sockets.delete(socket.uid);
            socket = null;
        });
        socket.once('error', e=>{
            socket.close();
        });
        socket.once('connect', ()=>{
            if(cb && typeof cb == 'function')
                cb();
        });
        setTimeout(()=>{
            socket.Connect();
        }, 100);
        return socket;
    }

    system.network.createServer = function(opts = {port: 0, timeout: 30000}, cb){
        if(system.network.servers[opts.port] != undefined){
            throw new Error('Port in use.');
        }
        let server = new NetServer(opts);
        system.network.servers[opts.port] = server;
        server.once('listen', ()=>{
            if(cb && typeof cb == 'function')
                cb();
        });
        server.once('close', ()=>{
            server = null;
            system.network.servers[opts.port] = undefined;
        });
        server.Listen();
        return server;
    }

    setInterval(()=>{
        system.processes.forEach(process=>process.Tick());
    }, 5);
    Object.defineProperty(system, 'prompt', {
        get: function(){
            return color(`${system.network.ssh != null ? '¬y(SSH)' : ''}¬1root@${system.network.address}¬w:¬B${system.cwd}¬W# `);
        }
    });

    system.killProcess = function(pid, sig){
        let process = system.processes.get(pid);
        if(!process) throw Error('PID invalid.');
        process.once('kill', ()=>{
            system.processes.delete(pid);
            process = null;
        });
        process.Kill(sig);
    }

    // Process class
    class Process extends EventEmitter{
        constructor(opts = {interval: 100, onStart: function(){}, onTick: function(){}, onKill: function(){}}){
            super();
            this.onStart = opts.onStart;
            this.onTick = opts.onTick;
            this.onKill = opts.onKill;
            this.started = false;
            this.internals = {
                tick: Date.now()+opts.interval,
                interval: opts.interval
            }
            this.pid = system.uuid();
        }
        Kill(sig){
            this.started = false;
            if(this.onKill != undefined && typeof this.onKill == 'function'){
                this.onKill.call(this, sig, this.pid);
            }
            this.emit('kill', sig, this.pid);
            system.events.process.emit('kill', sig, this.pid);
        }
        Start(){
            if(this.onStart != undefined && typeof this.onStart == 'function'){
                this.onStart.call(this, this.pid);
            }
            this.emit('start', this.pid);
            system.events.process.emit('start', this.pid);
            this.started = true;
        }
        Tick(){
            if(this.internals.tick <= Date.now() && this.started == true){
                this.internals.tick = Date.now()+this.internals.interval;
                if(this.onTick != undefined && typeof this.onTick == 'function'){
                    this.onTick.call(this);
                }
                this.emit('tick');
                system.events.process.emit('tick', this.pid);
            }
        }
    }

    system.createProcess = function(opts = {interval: 100, onStart: function(){}, onTick: function(){}, onKill: function(){}}, start = true){
        let process = new Process(opts);
        system.processes.set(process.pid, process);
        if(start) process.Start();
        return process;
    }

    class Service extends EventEmitter {
        constructor(service){
            super();
            this.service = service;
            this.stdout = "";
            this.sid = system.uuid();
            this.enabled = false;
            this.started = false;
            this.processes = [];
            this.listeners = new Map();
        }
        StartProcess(opts = {interval: 100, onStart: function(){}, onTick: function(){}, onKill: function(){}}){
            let proc = system.createProcess(opts);
            this.processes.push(proc.pid);
            proc.once('kill', (sig, pid)=>{
                let i = this.processes.findIndex(el=>pid==el);
                if(i != -1){
                    this.processes.splice(i, 1);
                }
            });
            return proc;
        }
        Log(text){
            this.stdout += `${this.stdout == "" ? '' : '\n'}[${new Date().toLocaleString()}] ${text}`;
        }
        Listen(event, cb){
            this.listeners.set(event, cb);
        }
        Status(){
            return this.stdout;
        }
        Enable(){
            this.enabled = true;
        }
        Disable(){
            this.enabled = false;
        }
        Start(){
            this.service.start.call(this, this.sid);
            this.started = true;
        }
        Stop(){
            this.service.stop.call(this);
            this.processes.forEach(pid=>{
                system.killProcess(pid, 0);
            });
            this.started = false;
        }
    }

    system.openEditor = function(path, data, resolve){
        system.terminal.print("¬GOpening editor...");
        system.editor = new BrowserWindow({width: 800, height: 600, webPreferences: {
            nodeIntegration: true
        }, show: false});
        system.editor.on('ready-to-show', ()=>{
            system.editor.show();
        });
        system.editor.on('close', ()=>{
            system.editor = null;
            system.terminal.print(`¬GEditor for ¬g${path} ¬Gclosed.`);
            resolve();
        });
        system.editor.on('show', ()=>{
            system.editor.webContents.send('editor', path, data);
        });
        system.editor.webContents.on('did-finish-load', ()=>{
            system.editor.setTitle(path);
            system.terminal.print(`¬GEditor for ¬g${path} ¬Gopened.`);
        });
        system.editor.loadFile('os/editor.html');
    }

    system.events.on('eval', ret=>{
        if(system.eval_callback != null){
            system.eval_callback(ret);
        } else {
            system.terminal.print(ret);
        }
    });

    system.events.on('run', ret=>{
        if(system.run_callback != null){
            system.run_callback(ret);
        } else {
            system.terminal.print(ret);
        }
    });

    system.events.commands.on('ssh_command', (command, resolve)=>{
        let cmd = command.split(' ')[0];
        let args = command.split(' ').slice(1);
        let c = system.commands.get(cmd);
        if(!c){
            system.run(system.resolve(command), (ret)=>{
                system.terminal.print(ret);
                system.systemd.forEach(service=>{
                    var list = service.listeners.get('command');
                    if(list) list(command);
                });
                resolve(ret);
            });
        } else {
            if(cmd == 'nano' || cmd == 'ssh' || cmd == 'colors' || cmd == 'clear') {
                resolve();
                return;
            }
            c(args, resolve);
        }
    });

    system.events.commands.on('command', (command, resolve)=>{
        command = command.replace(/\$CWD/g, system.cwd);
        let cmd = command.split(' ')[0];
        let args = command.split(' ').slice(1);
        if(cmd == '.reload'){
            system.terminal.print('¬gReloading commands...');
            loadCommands();
            system.terminal.print('¬gDone.');
            resolve();
            return;
        } else if(cmd == '.refresh'){
            system.network.sockets.forEach(socket=>{
                socket.close();
            });
            system.network.servers.filter(el=>el != undefined).forEach(server=>{
                server.Close();
            });
            system.processes.forEach(process=>{
                process.Kill(0);
            });
            system.systemd.forEach(el=>{
                el.Stop();
            });
            window.location.reload();
            return;
        }
        if(!system.network.ssh) {
            let c = system.commands.get(cmd);
            if(!c){
                system.run(system.resolve(command), (ret)=>{
                    system.terminal.print(ret);
                    system.systemd.forEach(service=>{
                        var list = service.listeners.get('command');
                        if(list) list(command);
                    });
                    resolve();
                });
            } else {
                c(args, resolve);
            }
        } else {
            system.network.ssh.send({op: 'run', data: command, id: system.uuid()}, (ret)=>{
                system.terminal.print(ret);
                resolve();
            });
        }
    });

    system.network.on('port_80', (source, buffer)=>{
        try {
            var dobj = JSON.parse(buffer.toString());
            if(dobj.response && dobj.id){
                var h = system.network.http.get(dobj.id);
                if(h){
                    h(dobj.response);
                }
            }
        } catch(e) {

        }
    });

    ipcRenderer.on('ws', (event, op, data)=>{
        var rx = new RegExp(/^\{[\s\S]*\}$|^\[[\s\S]*\]$/g);
        if(rx.test(data)){
            try {
                data = JSON.parse(data);
            } catch(e) {

            }
        }
        system.events.emit(op, data);
    });

    system.send = function(event, data){
        ipcRenderer.send('ws-send', event, data);
    }
    window.system = system;
    system.events.on('inbound', dobj=>{
        try {
            let source = dobj.source;
            let port = dobj.port;
            let buffer = Buffer.from(dobj.buffer);
            let d = JSON.parse(buffer.toString());
            if(d.uid){
                if(system.network.sockets.get(d.uid) && d.return){
                    let socket = system.network.sockets.get(d.uid);
                    if(socket.port == port && socket.host == source){
                        if(d.return == 'connect'){
                            socket.emit('connect');
                        } else if(d.return == 'close'){
                            socket.emit('close');
                        } else {
                            socket.emit('data', Buffer.from(d.data));
                        }
                    }
                } else if(system.network.servers[port] && d.op){
                    let server = system.network.servers[port];
                    server.Inbound(source, buffer);
                }
            } else {
                system.network.emit(`port_${port}`, source, buffer);
            }
        } catch(e) {
            console.log(e);
            console.log(Buffer.from(dobj.buffer).toString());
        }
    });
    console.log('Preload complete');
} catch(e) {
    console.log(e);
}