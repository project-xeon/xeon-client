const electron = require('electron');
const {BrowserWindow, app, ipcMain} =  electron;
const WebSocket = require('ws');
let ws;
const Path = require('path');
const fs = require('fs');
try {
    global.config = JSON.parse(fs.readFileSync('./config.json'))
} catch(e) {
    process.exit(1);
}

let win;

function send(event, data){
    if(win != null){
        win.webContents.send('ws', event, data);
    }
}

const RPC = require('discord-rpc');

var client = new RPC.Client({transport: 'ipc'});

client.on('ready', ()=>{
    console.log('Discord RPC ready.');
    client.setActivity({
        state: 'Connected',
        largeImageKey: 'icon',
        startTimestamp: new Date()
    });
});

function createWindow(){
    win = new BrowserWindow({width: 800, height: 600, webPreferences:{
        backgroundThrottling: false,
        sandbox: false,
        nodeIntegration: false,
        nodeIntegrationInWorker: false,
        devTools: true,
        contextIsolation: false,
        preload: Path.join(__dirname, 'preload.js')
    }, show: false, focusable: true, fullscreenable: true});
    win.loadURL(Path.normalize(`file://${app.getAppPath()}/os/index.html`));
    win.on('ready-to-show', ()=>{
        win.show();
    });
    win.on('show', ()=>{
        win.maximize();
        win.setAlwaysOnTop(true);
        win.focus();
        win.setAlwaysOnTop(false);
    });
    win.on('close', ()=>{
        win = null;
        app.exit();
    });
}

app.on('web-contents-created', (e, contents)=>{
    console.log('Contents created.');
    contents.on('will-navigate', (event, url)=>{
        if(url != `file://${app.getAppPath()}/os/index.html`) {
            console.log('Prevented travel to '+url);
            event.preventDefault();
        }
    });
});

app.on('ready', async ()=>{
    await client.connect('511590542627569694');
    let data = await client.request('AUTHORIZE', {client_id: '511590542627569694', scopes: ['rpc', 'rpc.api', 'identify']});
    ws = new WebSocket(config.ws);
    ws.on('open', ()=>{
        ws._send = ws.send;
        ws.send = function(event, data){
            ws._send(JSON.stringify({op: event, ts: Date.now(), data: data}));
        }
        ws.on('message', data=>{
            try {
                let dobj = JSON.parse(data);
                ws.emit(dobj.op, dobj.data);
                send(dobj.op, dobj.data);
            } catch(e) {

            }
        });
        ws.once('access', async token=>{
            await client.login({clientId: '511590542627569694', accessToken: token, scopes: ['rpc', 'rpc.api', 'identify']});
            delete token;
        });
        ws.once('auth', dobj=>{
            ipcMain.on('ws-send', (event, op, data)=>{
                ws.send(op, data);
            });
            createWindow();
            ws.send('ping');
        });
        ws.send('auth', {code: data.code});
    });
    ws.on('pong', ()=>{
        setTimeout(()=>{
            ws.send('ping');
        }, 20000);
    });
    ws.on('close', ()=>{
        if(win == null){
            throw new Error('Unauthorized');
        } else {
            send('ws-close');
        }
    });
});

app.on('window-all-closed', ()=>{
    app.exit();
});